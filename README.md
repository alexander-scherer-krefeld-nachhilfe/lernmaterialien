Als studierter Naturwissenschaftler, leistet Alexander Scherer seit 12 Jahren in Krefeld kompetente Nachhilfe in den Fächern Chemie, Physik, Biologie und Mathematik. Schwerpunktmäßig geht es um die Betreuung von Lernenden in der Oberstufe (Sek.II) sowie Universität. Der Unterricht erfolgt in der Regel bequem per Hausbesuch, oder in meinen Räumlichkeiten in Krefeld (Fischeln). Nach Absprache ist derzeit zudem Video-Unterricht via Skype oder Zoom möglich.

TERMINE:
Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com

Biologie-Ressourcen

"Rising Tide Biology"
Rising Tide Biology, verfasst vom Biologieprofessor Dr. Kevin Curran von der University of San Diego, bietet umfassende Zusammenfassungen einiger der aufregendsten und bahnbrechendsten Gebiete der modernen Biologie.

"iBiology"
Die Mission von iBiology besteht darin, in Form von frei zugänglichen, kostenlosen Videos die Aufregung der modernen Biologie und den Prozess, durch den wissenschaftliche Entdeckungen gemacht werden, zu vermitteln. Ziel ist es, Sie mit den führenden Wissenschaftlern der Biologie in Kontakt zu bringen, damit Sie herausfinden können, wie sie über wissenschaftliche Fragen denken und ihre Forschungen durchführen, und einen Eindruck von ihrer Persönlichkeit, ihren Meinungen und Perspektiven bekommen können.

Life Sciences Outreach-Programm der Harvard Universität
Auf der Website der Harvard University werden zwei bemerkenswerte biologische Animationen präsentiert - eine über "Das Innenleben der Zelle" und die andere über "Mitochondrien".

"Understanding Evolution"
Die Website, Understanding Evolution for Teachers, bietet umfassende Unterrichtsressourcen zu diesem Thema

"BenchFly"
Mit Labortechniken, wissenschaftlichen Videos und aktuellen Protokollen ist BenchFly der alltägliche Leitfaden für eine Karriere als Wissenschaftler.

Lebensbaum-Webprojekt
Das Tree of Life Web Project (ToL) ist eine Gemeinschaftsarbeit von Biologen und Naturliebhabern aus der ganzen Welt. Auf mehr als 10.000 Seiten im World Wide Web bietet das Projekt Informationen über die biologische Vielfalt, die Eigenschaften verschiedener Organismengruppen und ihre Evolutionsgeschichte (Phylogenie).

"Secrets of the Sequence"
Die "Secrets of the Sequence Video Series" bietet Lehrerinnen und Lehrern eine innovative Möglichkeit, erstaunliche Fortschritte in den Biowissenschaften aus der Genforschung in ihren Unterricht zu integrieren.

Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com

"Molecular Workbench"
Diese Website bietet eine Sammlung interaktiver Simulationen und Lernaktivitäten, die mit Hilfe der Molekularen Werkbank erstellt wurden. Alle Exponate sind die Ergebnisse von Berechnungen, die auf wissenschaftlichen Prinzipien basieren.

Annenberg-Lerner
Minds of Our Own: Eine Videodokumentation über Bildung und Lernen für Sek. II und Stiudium. Pädagogen und Eltern; 3 einstündige Videoprogramme und ein Leitfaden. Dieser Dokumentarfilm deckte viele studentische Missverständnisse auf.

Allen Institute Ressourcen für Lehrer 
Diese kostenlosen Ressourcen des Allen-Instituts richten sich an Biologielehrer an Gymnasien und Universitäten, die echte Forschungserfahrungen für ihre Schüler suchen. Anpassbare Unterrichtspläne führen die Schüler mit Hilfe von geführten Experimenten, die Daten aus den laufenden Forschungsprojekten des Instituts verwenden, in grundlegende Konzepte der Biologie ein.

Unverbindliche Terminvereinbarung bequem online:
Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com

---

Physik-Ressourcen

Newtons Bewegungsgesetze und Auto-Physik
Diese Ressource mit dem Titel Newton's Laws of Motion and Car Physics (Newtons Bewegungsgesetze und Autophysik) bietet eine große Menge an interessantem Lehrmaterial zu jedem der drei Gesetze.

Universe and More
Interaktive Physikspiele, die von einem AP-Physiklehrer erstellt wurden.

ShareMyLesson
Diese von ShareMyLesson gehostete Website bietet fast 2000 Physik-Ressourcen, darunter Aktivitäten, Arbeitsblätter, Spiele, Unterrichtspläne, Rätsel, Poster, Präsentationen, Beurteilungen und andere Ideen, die Sie mit Ihren Schülern verwenden können.

PhysLink.com
PhysLink.com ist eine umfassende Online-Website für Ausbildung, Forschung und Nachschlagewerke im Bereich Physik und Astronomie. PhysLink.com bietet nicht nur qualitativ hochwertige Inhalte, sondern ist auch ein Treffpunkt für Fachleute, Studenten und andere neugierige Geister.

Unverbindliche Terminvereinbarung bequem online:
Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com

Das Physik-Klassenzimmer
Das Physik-Klassenraum-Tutorial ist ein Online-Physik-Tutorial, das für Physikstudenten der Oberstufe geschrieben wurde. Das Tutorial behandelt grundlegende Physikthemen mit Hilfe von informativen Grafiken und einer leicht verständlichen Sprache. Jede Einheit ist in Lektionen und Unterlektionen unterteilt.

Das Teilchen-Abenteuer
Eine preisgekrönte interaktive Tour zu Quarks, Neutrinos, Antimaterie, Extradimensionen, dunkler Materie, Beschleunigern und Teilchendetektoren der Particle Data Group des Lawrence Berkeley National Laboratory.

Einstein Online
Einstein Online ist ein Webportal mit Informationen über Albert Einsteins Relativitätstheorien und ihre coolsten Anwendungen, von den kleinsten Teilchen über Schwarze Löcher bis hin zur Kosmologie.

Institut für Physik: Praktische Physik
Experimente können die Beobachtungsgabe der Schülerinnen und Schüler schärfen, Fragen anregen und dazu beitragen, neues Verständnis und Vokabular zu entwickeln. Diese Website richtet sich an Physiklehrerinnen und Physiklehrer und ermöglicht es ihnen, ihre Fähigkeiten und Erfahrungen mit der Durchführung von Experimenten im Klassenzimmer zu teilen.

Unverbindliche Terminvereinbarung bequem online:
Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com

PhET Interactive Simulations
PhET Interactive Simulations ist ein fortwährendes Bestreben, eine umfangreiche Reihe von Simulationen bereitzustellen, um die Art und Weise zu verbessern, wie Physik, Chemie, Biologie, Geowissenschaften und Mathematik gelehrt und gelernt werden. Bei den Simulationen handelt es sich um interaktive Werkzeuge, die es den Schülerinnen und Schülern ermöglichen, Verbindungen zwischen Phänomenen des realen Lebens und der zugrunde liegenden Wissenschaft herzustellen, die solche Phänomene erklärt.

ComPADRE Digital Library
Die ComPADRE Digital Library ist ein Netzwerk kostenloser Online-Ressourcensammlungen zur Unterstützung von Dozenten, Studenten und Lehrern im Physik- und Astronomieunterricht.

Physik is Phun
Physics is Phun listet Websites für Lehrerinnen und Lehrer auf, die eine Vielzahl interaktiver Simulationen, Experimente und Ideen bieten - allesamt darauf ausgerichtet, den Schülerinnen und Schülern ein besseres Verständnis der zugrunde liegenden Konzepte der Physik zu vermitteln.

Unverbindliche Terminvereinbarung bequem online:
Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com
